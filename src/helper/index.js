import Cookies from 'js-cookie';

export function currentLang(){
  let current_lang = "EN";
  if(typeof window !== 'undefined') {
    if (window.location.href.indexOf("az") > -1) {
      current_lang = "AZ"
    } else if (window.location.href.indexOf("en") > -1) {
      current_lang = "EN"
    } else if (window.location.href.indexOf("ru") > -1) {
      current_lang = "RU"
    }
  }
  return current_lang;
}
  // Cookies.get('pt_lang')
  //   ? Cookies.get('pt_lang')
  //   : getDefaultLanguage();

export const getAllLangs = [
  {'id' : 1,'name' : 'AZ' , 'lang' : 'az'},
  {'id' : 2,'name' : 'EN' , 'lang' : 'en'},
  {'id' : 3,'name' : 'RU' , 'lang' : 'ru'}
]

export function handleLangChange(e,lang) {
  e.preventDefault();
  e.stopPropagation();
  Cookies.set('pt_lang',lang);
  if(typeof window !== 'undefined') window.location.href = `/${lang}`;
}

export const menuItems = [
    {'id' : 1, 'name' : 'about'},
    {'id' : 2, 'name' : 'features'},
    {'id' : 3, 'name' : 'pricing'},
    {'id' : 4, 'name' : 'mobile'}
  ]


export const packages = [
  {'id' : 1, 'name' : 'Task Free','price' : '0','discount' : '','includes' : [
      {'id' : 1,'text' : 'The number of employees : ','amount' : '3'},
      {'id' : 2,'text' : 'Capacity in MB : ','amount' : '100MB'},
      {'id' : 3,'text' : 'Written tasks','amount' : ''},
      {'id' : 4,'text' : 'Kanban boards','amount' : ''},
      {'id' : 5,'text' : 'Assignment of deadlines','amount' : ''},
      {'id' : 6,'text' : 'Short tasks','amount' : ''},
      {'id' : 7,'text' : 'Management of the task execution process','amount' : ''}
    ]
  },
  {'id' : 2, 'name' : 'Task Standart','price' : '3.9','discount':'6.9','includes' : [
      {'id' : 1,'text' : 'The number of employees : ','amount' : '10'},
      {'id' : 2,'text' : 'Capacity in MB : ','amount' : '500MB'},
      {'id' : 3,'text' : 'All the features of the Task Free package','amount' : ''},
      {'id' : 4,'text' : 'Voice tasks','amount' : ''},
      {'id' : 5,'text' : 'Delayed tasks','amount' : ''},
      {'id' : 6,'text' : 'Link to the project','amount' : ''}
    ]
  },
]


export const browserLanguage = 'en';
  // typeof window !== 'undefined' && (window.navigator.userLanguage || window.navigator.language);

export function getDefaultLanguage() {
  getAllLangs.map(value => {
    if (value.lang === browserLanguage){
      return browserLanguage
    }
    else{
      return "en"
    }
  })
}

// export function options(){
//   return {
//     // language JSON resource path
//     path: `${__dirname}/src/intl`,
//     // supported language
//     languages: [`en`, `ru`, `az`],
//     // language file path
//     defaultLanguage: Cookies.get('pt_lang'),
//     // option to redirect to `/ko` when connecting `/`
//     redirect: true,
//   }
// }

export function textLengther(txt){
  return txt.substring(0,70)
}