import React from "react"
import "../assets/sass/layout/header.scss"
import Logo from "../containers/logo/Logo"
import MenuLister from "../containers/menu/MenuLister"
import { menuItems } from "../helper"
import LangContainer from "../containers/lang/LangContainer"
import LoginButton from "../containers/button/LoginButton"
import MenuButton from "../containers/button/MenuButton"
import MenuDivider from "../containers/divider/MenuDivider"

const Header = ({ menuStatus, setMenuStatus }) => {

  const handleMenuOpen = () => {
    setMenuStatus(!menuStatus)
  }

  return (
    <div className="columns">
      <div className="column">
        <Logo menuStatus={menuStatus}/>
      </div>
      <div className="column">
        <MenuLister list={menuItems}/>
        <MenuDivider/>
        {/*<LangContainer/>*/}
        <LoginButton/>
        <MenuButton handleMenuOpen={handleMenuOpen} menuStatus={menuStatus}/>
      </div>
    </div>
  )
}

export default Header