import React from "react"
import logo from "../images/logo_black.svg"
import "../assets/sass/layout/footer.scss"
import { Link } from "gatsby"

const Footer = () => {
  return (
    <div className={"footer"}>
      <Link to={"/"} style={{
        textDecoration: 'none',
        color: '#363636'
      }}>
        <div className={"logo"}>
          <img src={logo} alt="protask.az home page"/>
          <div className={"text"}><span>PRO</span>TASK</div>
        </div>
      </Link>
      <hr className={"divider"}/>
      <div className={"socials"}>
        <div className={"social youtube"}>
          <a className={"youtube_bg"} href="https://www.youtube.com/channel/UC_bE77S1wH_2SM3cweD-LQA"/>
        </div>
        {/*<div className={"social twitter"}>*/}
        {/*  <a className={"twitter_bg"} href="/"/>*/}
        {/*</div>*/}
        <div className={"social linkedin"}>
          <a className={"linkedin_bg"} href="https://www.linkedin.com/company/prospecterp/?viewAsMember=true"/>
        </div>
        <div className={"social facebook"}>
          <a className={"facebook_bg"} href="https://www.facebook.com/PronetERP/"/>
        </div>
        <div className={"social instagram"}>
          <a className={"instagram_bg"} href="https://www.instagram.com/pronet_erp_smb/"/>
        </div>
      </div>
      <div className={"privacy"}>
        &copy; 2019 Privacy — Terms
      </div>
    </div>
  )
}

export default Footer