import React, { useEffect, useState } from "react"
import img1 from "../images/img1.svg"
import img2 from "../images/img2.svg"

import voice from "../images/voice.svg"
import photo from "../images/photo.svg"
import computer from "../images/computer.svg"
import img_cap from "../images/img_cap.svg"
import Slider from "../pages/slider"
import PurposeContainer from "../containers/section/PurposeContainer"
import FeatureContainer from "../containers/section/FeatureContainer"
import Trans from "../components/trans"
import CapabilityContainer from "../containers/section/CapabilityContainer"
import PackageContainer from "../containers/section/PackageContainer"
import MobileContainer from "../containers/section/MobileContainer"

const Content = ({ props }) => {

  const [openCap, setOpenCap] = useState(1)

  const handleCapClick = (id) => {
    if (id === openCap) {
      setOpenCap(0)
    } else {
      setOpenCap(id)
    }
  }

  useEffect(() => {
    let hashRoute = window.location.hash.substr(1);
    let element = document.getElementById(hashRoute);
    if (element){
      element.scrollIntoView({block: "center", behavior: "smooth"});
    }
  },[]);

  return (
    <React.Fragment>
      <div className="columns">
        <div className="column is-full slider-container">
          <div className="container">
            <Slider/>
            <span style={{display:"block",marginBottom : 45}}/>
          </div>
        </div>
      </div>
      <div className="columns" style={{ marginLeft: 10 }}>
        <div className="column is-full">
          <div className="container">
            <PurposeContainer img={img1}
                              title={<Trans>what.title</Trans>}
                              description={<Trans>what.description</Trans>}
                              information={<Trans>what.information</Trans>}
            />
            <PurposeContainer img={img2}
                              title={<Trans>purpose.title</Trans>}
                              description={<Trans>purpose.description</Trans>}
                              information={<Trans>purpose.information</Trans>}
                              reverse={true}
            />
            <div className="columns">
              <div className="column is-full">
                <div className={"yl-content"} style={{ width: "100%" }}>
                  <h3 className={"header feature"} style={{ justifyContent: "center", marginBottom: 40 }}>
                    <Trans>feature.header</Trans>
                  </h3>
                  <div className="columns yp-content">
                    <FeatureContainer
                      img={voice}
                      title={<Trans>feature.voice.title</Trans>}
                      desc={<Trans>feature.voice.desc</Trans>}
                    />
                    <FeatureContainer
                      img={photo}
                      title={<Trans>feature.photo.title</Trans>}
                      desc={<Trans>feature.photo.desc</Trans>}
                    />
                    <FeatureContainer
                      img={computer}
                      title={<Trans>feature.link.title</Trans>}
                      desc={<Trans>feature.link.desc</Trans>}
                    />
                  </div>
                </div>
              </div>
            </div>

            <div className="columns">
              <div className="column is-full">
                <div className={"yl-content"} style={{ width: "100%", marginTop: 90 }}>
                  <h3 className={"header feature"} style={{ justifyContent: "center", marginBottom: 10 }}>
                    Capabilities of the system
                  </h3>
                  <div className="columns yp-content">
                    <div className="column">
                      <img src={img_cap} alt="protask.az - capabilities of the system"/>
                    </div>
                    <div className="column">
                      <CapabilityContainer
                        title={"Manage all your urgent tasks from your smartphone."}
                        desc={"Always keep in touch with your team either from your workplace through your PC, or mobile device while you are traveling through instant communication opportunities "}
                        id={1}
                        openCap={openCap}
                        handleClick={handleCapClick}
                      />
                      <CapabilityContainer
                        title={"Manage all your urgent tasks from your smartphone."}
                        desc={"Keep your employees updated on regular tasks through instant notifications."}
                        id={2}
                        openCap={openCap}
                        handleClick={handleCapClick}
                      />
                      <CapabilityContainer
                        title={"Manage all your urgent tasks from your smartphone."}
                        desc={"The system allows you to observe your team's work through “Kanban” boards. You will be able to easily plan your tasks, simplify assignment regulations, automate workflows, schedule and improve the quality of the project process.  "}
                        id={3}
                        openCap={openCap}
                        handleClick={handleCapClick}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <span id={"pricing"}/>
            <div className="columns">
              <div className="column is-full">
                <PackageContainer/>
              </div>
            </div>

            <div className="columns about" id={"mobile"}>
              <MobileContainer/>
            </div>

          </div>
        </div>
      </div>
    </React.Fragment>
  )
}

export default Content