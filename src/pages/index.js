import React, { useState } from "react"
import "../assets/app.scss"
import Header from "../layouts/header"
import classNames from "classnames"
import Menu from "./menu"
import Content from "../layouts/content"
import Footer from "../layouts/footer"
import SEO from "../components/seo"

const IndexPage = () => {
  const [menuStatus, setMenuStatus] = useState(false)

  return (
    <div className="main">
      <SEO
        title={"Protask.az"}
        description={"ProTask, an easy-to-use, intuitive and secure task management software, lets you check the current state of projects and tasks execution with online data flow through the app on your phone and assign various tasks to your employees in written, graphic, or audio form."}
        type={"website"}
        android_app_name={"ProTask"}
        android_app_package={"com.prospect"}
        author={"axmedbek"}
        image={"https://lh3.googleusercontent.com/s1CMpPREbFvzgBqWZKQU7s93OQnIQRaLivLBDveJRRpCH9LVWb4IGMsLmNekYC8nVg=s180-rw"}
        keywords={"Task management software,Online Execution Processes,Task tracking,Automation of tasks,Task report,Voice tasks,Graphic photo tasks,Task and execution processes,Execution process management,Task manager app free,Small and medium business,Enterprise resource planning,Business processes,To do list,Workflow management,Execution tracker,Project management software,Kanban board,Project management,Task organizer free,Online team work"}
        site_name={"protask.az"}
        url={"http://protask.az"}
      />
      <div className="fixed-header">
        <div className={classNames("column is-full", !menuStatus ? "header-container" : "header-container-white")}>
          <div className="container">
            <Header menuStatus={menuStatus} setMenuStatus={setMenuStatus}/>
          </div>
        </div>
      </div>
      <div className="columns" style={{ marginTop: 70 }}>
        <div className="column">
          <div className={"slider"}>
            {
              menuStatus ? <Menu setMenuStatus={setMenuStatus} menuStatus={menuStatus}/> : <Content/>
            }
          </div>
        </div>
      </div>

      <Footer/>
    </div>
  )
}

export default IndexPage