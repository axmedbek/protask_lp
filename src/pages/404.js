import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"

const NotFoundPage = () => {

  if (typeof window !== 'undefined') {
    window.location = '/';
  }

  return null;
}

export default NotFoundPage
