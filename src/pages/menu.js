import React, { useEffect } from "react"
import "../assets/sass/layout/menu.scss"
import { Link } from "gatsby"
import Trans from "../components/trans"
import PLink from "../components/button/Button"
import { menuItems } from "../helper"

const Menu = ({ setMenuStatus,menuStatus }) => {

  useEffect(() => {
    window.scroll(0,0)
  }, []);

  const handleMenuClick = () => {
    setMenuStatus(!menuStatus)
  };

  return (
    <div className={"columns mobile-menu"}>
      {
        menuItems.map(value => (
          <div key={value.id} className="column is-full item">
            <Link to={`/en/#${value.name}`} onClick={handleMenuClick}><Trans>{value.name}</Trans></Link>
          </div>
        ))
      }
      <div className="column is-full item">
        <PLink className={"sign-btn"} text={"sign-in"} link={"https://protask.az/login"}/>
      </div>
    </div>
  )
}

export default Menu