import React from "react"
import '../assets/sass/layout/slider.scss';
import phone from '../images/mobile.svg';
import notebook from '../images/notebook.svg';
import Trans from '../components/trans'

const Slider = () => {
  return (
    <div className="columns">
      <div className="column slider-content">
        <div className="columns">
          <div className="column is-full title">
            PROTASK
          </div>
        </div>
        <div className="columns">
          <div className="column is-full description">
            <Trans>slider.title</Trans>
          </div>
        </div>
        <div className="columns">
          <div className="column is-full information">
            <Trans>slider.description</Trans>
          </div>
        </div>
        <div className="columns">
          <div className="column is-full try-btn-container">
            <a href={"https://protask.az/login"} className={"try-btn"} title={"protask.az try free"}><Trans>slider.try</Trans></a>
          </div>
        </div>
      </div>
      <div className="column slider-img">
        <img src={notebook} alt="protask notebook version" className={"notebook-version"}/>
        <img src={phone} alt="protask phone version" className={"phone-version"}/>
      </div>
    </div>
  )
}

export default Slider