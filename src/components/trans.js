import React from "react"
import { injectIntl } from "gatsby-plugin-intl"

const Trans = ({ intl,children }) => {
  return intl.formatMessage({ id: children })
}

export default injectIntl(Trans);
