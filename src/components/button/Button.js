import React from "react"
import Trans from "../trans"
import styles from './button.module.scss'
import classNames from 'classnames'

const PLink = ({ text,link,className,...rest}) => {
  return (
    <a href={link} className={classNames(styles.btn,className)} {...rest}>
      <Trans>{text}</Trans>
    </a>
  )
}

export default PLink