import React from "react"
import Trans from "../../components/trans"
import styles from './menu.module.scss';
import { Link } from 'gatsby'

const MenuItem = ({ value }) => {
  return (
    <li className={styles.menuItem}>
      <Link to={`/en/#${value.name}`}><Trans>{value.name}</Trans></Link>
    </li>
  )
}

export default MenuItem