import React from "react"
import MenuItem from "./MenuItem"
import styles from './menu.module.scss';

const MenuLister = ({ list }) => {
  return (
    <ul className={styles.menu}>
      {
        list.map(value => (
          <MenuItem value={value} key={value.id}/>
        ))
      }
    </ul>
  )
}

export default MenuLister