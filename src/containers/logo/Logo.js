import React from "react"
import { Link } from "gatsby"
import logo from "../../images/protask_logo.svg"
import logo_white from "../../images/logo_white.svg"
import styles from './logo.module.scss';

const Logo = ({ menuStatus }) => {
  return (
    <Link to="/">
      {
        !menuStatus
          ? <img src={logo} alt="protask.az logo" className={styles.logo}/>
          : <img src={logo_white} alt="protask.az logo" className={styles.logo}/>
      }
    </Link>
  )
}

export default Logo