import React from "react"
import styles from './divider.module.scss'

const MenuDivider = () => {
  return (
    <div className={styles.menuDivider}/>
  )
}

export default MenuDivider