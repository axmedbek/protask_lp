import React from "react"
import styles from "./lang.module.scss"
import LangItem from "./LangItem"

const LangLister = ({ list }) => {
  return (
    <ul className={styles.select}>
      {
        list.map(value => (
          <LangItem name={value.name} lang={value.lang} key={value.id}/>
        ))
      }
    </ul>
  )
}

export default LangLister