import React from "react"
import { getAllLangs, currentLang } from "../../helper"
import styles from "./lang.module.scss"
import LangLister from "./LangLister"
import SelectedLang from "./SelectedLang"

const LangContainer = () => {
  return (
    <div className={styles.langContainer}>
      <SelectedLang lang={currentLang()}/>
      <LangLister list={getAllLangs}/>
    </div>
  )
}

export default LangContainer