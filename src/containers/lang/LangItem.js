import React from "react"
import { Link } from "gatsby"
import styles from "./lang.module.scss"
import classNames from "classnames"


const LangItem = ({ lang, name }) => {
  return (
    <li className={classNames(styles.item, typeof window !== 'undefined' && window.location.href.indexOf(lang) > -1 && styles.active)}>
      <Link to={`/${lang}`}>{name}</Link>
    </li>
  )
}

export default LangItem