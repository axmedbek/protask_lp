import React from "react"
import styles from "./lang.module.scss"
import arrow from "../../images/caret-down.png"

const SelectedLang = ({ lang }) => {
  return (
    <div className={styles.lang}><span className={styles.span}>{lang}</span>
      <img src={arrow} alt="arrow down" className={styles.img}/>
    </div>
  )
}

export default SelectedLang