import React from "react"
import plus from "../../images/add.svg"
import minus from "../../images/minus.svg"
import { textLengther } from "../../helper"

const CapabilityContainer = ({ title,desc,openCap,id,handleClick }) => {
  return (
    <div className="columns capability" style={{ marginTop: 20 }}>
      <div className="column is-11 is-mobile">
        <h4 className={"title"}>{title}</h4>
        <p className={"desc"}>{openCap === id ? desc : textLengther(desc)}</p>
      </div>
      <div className="column control is-mobile" onClick={() => handleClick(id)}>
        {
          openCap === id
            ? <img src={minus} alt="protask.az - show capabilities"/>
            : <img src={plus} alt="protask.az - hide capabilities"/>
        }
      </div>
    </div>
  )
}

export default CapabilityContainer