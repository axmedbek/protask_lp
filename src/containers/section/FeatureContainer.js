import React from "react"

const FeatureContainer = ({ img,title,desc }) => {
  return (
    <div className="column">
      <img src={img} alt="protask.az features provided by the system" style={{backgroundColor: '#fafafa'}}/>
      <h4 className={"title"}>{title}</h4>
      <p className={"description"}>{desc}</p>
    </div>
  )
}

export default FeatureContainer