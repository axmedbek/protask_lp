import React from "react"

const PurposeContainer = ({ img, title, description, information, reverse }) => {
  return (
    <React.Fragment>
      {
        reverse ?
        <div className="columns">
          <div className="column is-half">
            <div className={"yl-content"}>
              <h3 className={"header"}>{title}</h3>
              <hr style={{ background: "#BDBDBD" }}/>
              <h5 className={"pg1"}>{description}</h5>
              <h6 className={"pg2"}>{information}</h6>
            </div>
          </div>
          <div className="column is-half">
            <img src={img} alt="protask.az - what is for"/>
            <span id={"features"} style={{display:"block",marginBottom : 20}}/>
          </div>
        </div>
          :
          <div className="columns">
            <span id={"about"}/>
            <div className="column is-half">
              <img src={img} alt="protask.az - what is for"/>
            </div>
            <div className="column is-half">
              <div className={"yl-content"}>
                <h3 className={"header"}>{title}</h3>
                <hr style={{ background: "#BDBDBD" }}/>
                <h5 className={"pg1"}>{description}</h5>
                <h6 className={"pg2"}>{information}</h6>
              </div>
            </div>
          </div>
      }
    </React.Fragment>
  )
}

export default PurposeContainer