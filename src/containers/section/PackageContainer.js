import React from "react"
import PackageList from "../package/PackageList"

const PackageContainer = () => {
  return (
    <div className={"yl-content"} style={{ width: "100%" }}>
      <h3 className={"header"} style={{ justifyContent: "center" }}>Pricing</h3>
      <h5 className={"detail pricing-detail"}>You and your team can use the trial
        version of PROTask app with unlimited capabilities for
        <span className={"day"}> 14 days </span> for free
        and choose the convenient package after the trial ends. </h5>
      <div className="columns yp-content" style={{ marginTop: 20 }}>
        <PackageList/>
        <div className="column more_comp_package">
          <div className={"more_comp"}>
            More compliant options coming soon
          </div>
        </div>
      </div>
    </div>
  )
}

export default PackageContainer