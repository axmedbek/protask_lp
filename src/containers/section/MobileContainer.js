import React from "react"
import about from "../../images/about.svg"
import google from "../../images/google.svg"
import apple from "../../images/apple.svg"

const MobileContainer = () => {
  return (
    <React.Fragment>
      <div className="column platforms" style={{marginLeft : 200}}>
        <img src={about} alt="protask.az mobile application for all platforms"/>
      </div>
      <div className="column information" style={{marginRight : 150}}>
        <h3 className={"header"} style={{marginTop: 50}}>Use ProTask from anywhere with our Android and iOS applications.</h3>
        <p className={"description"}>You and your team can use the PROTask app for 14 days for free. No card
          information is required.</p>
        <div className={"mobiles"}>
          <a href="https://play.google.com/store/apps/details?id=com.prospect"
             title={"protask.az mobile application for android"}>
            <img src={google} alt="protask.az google play application"/>
          </a>
          <a href="https://apps.apple.com/us/app/protask/id1453517107?ls=1"
             title={"protask.az mobile application for ios"}>
            <img src={apple} alt="protask.az apple store application"/>
          </a>
        </div>
      </div>
    </React.Fragment>
  )
}

export default MobileContainer