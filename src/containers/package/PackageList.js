import React from "react"
import { packages } from "../../helper"
import Package from "./Package"

const PackageList = () => {
  return (
    <React.Fragment>
      {
        packages.map(my_package => (
          <div key={my_package.id} className="column package">
            <Package my_package={my_package}/>
          </div>
        ))
      }
    </React.Fragment>
  )
}

export default PackageList