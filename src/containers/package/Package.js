import React from "react"
import PackageInclude from "./PackageInclude"

const Package = ({ my_package }) => {
  return (
    <React.Fragment>
      <h3 className={"title"}>{my_package.name}</h3>
      <div className={"price"}>
        <div className={"currency"}>$</div>
        <div className={"amount"}>{my_package.price}</div>
        <div className={"discount"}>{my_package.discount}</div>
      </div>
      <div className={"includes"}>
        <h5 className={"title"}>Package includes</h5>
        {
          my_package.includes.map(value => (
            <PackageInclude key={value.id} text={value.text} amount={value.amount}/>
          ))
        }
        <div className="include">
          <a href="https://protask.az/login" className={"select-btn"}>Select this package</a>
        </div>
      </div>
    </React.Fragment>
  )
}

export default Package