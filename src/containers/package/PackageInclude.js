import React from "react"
import check from "../../images/check.svg"

const PackageInclude = ({ text , amount }) => {
  return (
    <div className="include">
      <img src={check} alt="icon"/>
      <h6 className={"detail"}>{text}</h6>
      <h6 className={"amount"}>{amount}</h6>
    </div>
  )
}

export default PackageInclude