import React from "react"
import burger from "../../images/burger.svg"
import close from "../../images/close.svg"
import './button.scss'

const MenuButton = ({ menuStatus,handleMenuOpen }) => {
  return (
    <div className={"burger-menu"} onClick={handleMenuOpen}>
      {
        !menuStatus
          ? <img src={burger} alt="protask.az menu icon"/>
          : <img src={close} alt="protask.az menu icon" className={"close"}/>
      }
    </div>
  )
}

export default MenuButton