import React from "react"
import PLink from "../../components/button/Button"
import './button.scss'

const LoginButton = () => {
  return (
   <PLink className={"sign-in-btn"} text={"sign-in"} link={"https://protask.az/login"}/>
  )
}

export default LoginButton